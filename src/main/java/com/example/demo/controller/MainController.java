/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.controller;

import com.example.demo.entity.Attendance;
import com.example.demo.entity.Employees;
import com.example.demo.entity.Position;
import com.example.demo.repository.AttendanceRepo;
import com.example.demo.repository.EmployeesRepo;
import com.example.demo.repository.PositionRepo;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lenovo
 */
@RestController
@RequestMapping("/api")
public class MainController {

    @Autowired
    private AttendanceRepo attendanceRepo;

    @Autowired
    private EmployeesRepo employeesRepo;

    @Autowired
    private PositionRepo positionRepo;

    @RequestMapping("/getallatt")
    public List<Attendance> getAllAtt() {
        List<Attendance> attendances = new ArrayList<>();
        attendances = (List<Attendance>) attendanceRepo.findAll();
        return attendances;
    }

    @RequestMapping("/getattid")
    public Attendance getAllAttId(@RequestParam Integer id) {
        Attendance attendance = new Attendance();
        attendance = attendanceRepo.findByAttenId(id);
        return attendance;
    }

    @RequestMapping("/getbyempid")
    public List<Attendance> getAllEmpId(@RequestParam Integer empId) {
        List<Attendance> attendances = new ArrayList<>();
        attendances = attendanceRepo.findByEmpID(empId);
        return attendances;
    }

    @PostMapping("/saveatt")
    public Map<String, String> saveAtt(@Valid @RequestBody Attendance a) {
        Map<String, String> respMsg = new LinkedHashMap<>();
        Attendance attendance = new Attendance();
        Date date = new Date();
        attendance = attendanceRepo.save(a);
        if (attendance.getAttenId() != null) {
            respMsg.put("attenId", attendance.getAttenId().toString());
            respMsg.put("cosetattde", HttpStatus.OK + "");
            respMsg.put("message", "success");
        } else {
            respMsg.put("attenId", "");
            respMsg.put("code", HttpStatus.CONFLICT + "");
            respMsg.put("message", "failed");
        }
        return respMsg;
    }

    @PostMapping("/deleteattid")
    public Map<String, String> deleteAttId(@Valid @RequestBody Attendance a) {
        Map<String, String> respMsg = new LinkedHashMap<>();
        attendanceRepo.delete(a);
        respMsg.put("attenId", a.getAttenId().toString());
        respMsg.put("cosetattde", HttpStatus.OK + "");
        respMsg.put("message", "success");
        return respMsg;
    }

    @RequestMapping("/getallemp")
    public List<Employees> getAllEmp() {
        List<Employees> employeeses = new ArrayList<>();
        employeeses = (List<Employees>) employeesRepo.findAll();
        return employeeses;
    }

    @RequestMapping("/getempbyid")
    public Employees getEmpId(@RequestParam Integer empId) {
        Employees employees = new Employees();
        employees = employeesRepo.findByEmpID(empId);
        return employees;
    }

    @RequestMapping("/getempbyposid")
    public List<Employees> getAllPosId(@RequestParam Integer posId) {
        List<Employees> employeeses = new ArrayList<>();
        employeeses = employeesRepo.findByPosID(posId);
        return employeeses;
    }

    @PostMapping("/saveemp")
    public Map<String, String> saveEmp(@Valid @RequestBody Employees e) {
        Map<String, String> respMsg = new LinkedHashMap<>();
        Employees employees = new Employees();
        employees = employeesRepo.save(e);
        if (employees.getEmpID() != null) {
            respMsg.put("attenId", employees.getEmpID().toString());
            respMsg.put("cosetattde", HttpStatus.OK + "");
            respMsg.put("message", "success");
        } else {
            respMsg.put("attenId", "");
            respMsg.put("code", HttpStatus.CONFLICT + "");
            respMsg.put("message", "failed");
        }
        return respMsg;
    }

    @PostMapping("/deleteempid")
    public Map<String, String> deleteEmpId(@Valid @RequestBody Employees e) {
        Map<String, String> respMsg = new LinkedHashMap<>();
        employeesRepo.delete(e);
        respMsg.put("attenId", e.getEmpID().toString());
        respMsg.put("cosetattde", HttpStatus.OK + "");
        respMsg.put("message", "success");
        return respMsg;
    }

    @RequestMapping("/getallpos")
    public List<Position> getAllPos() {
        List<Position> positions = new ArrayList<>();
        positions = (List<Position>) positionRepo.findAll();
        return positions;
    }

    @RequestMapping("/getposbyid")
    public Position getPosId(@RequestParam Integer posId) {
        Position position = new Position();
        position = positionRepo.findByPosID(posId);
        return position;
    }

    @PostMapping("/savepos")
    public Map<String, String> savePos(@Valid @RequestBody Position p) {
        Map<String, String> respMsg = new LinkedHashMap<>();
        Position position = new Position();
        position = positionRepo.save(p);
        if (position.getPosID() != null) {
            respMsg.put("attenId", position.getPosID().toString());
            respMsg.put("cosetattde", HttpStatus.OK + "");
            respMsg.put("message", "success");
        } else {
            respMsg.put("attenId", "");
            respMsg.put("code", HttpStatus.CONFLICT + "");
            respMsg.put("message", "failed");
        }
        return respMsg;
    }

    @PostMapping("/deleteposid")
    public Map<String, String> deletePosId(@Valid @RequestBody Position p) {
        Map<String, String> respMsg = new LinkedHashMap<>();
        positionRepo.delete(p);
        respMsg.put("attenId", p.getPosID().toString());
        respMsg.put("cosetattde", HttpStatus.OK + "");
        respMsg.put("message", "success");
        return respMsg;
    }

}

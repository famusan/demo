/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.repository;

import com.example.demo.entity.Attendance;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Lenovo
 */
@Repository
public interface AttendanceRepo extends CrudRepository<Attendance, Integer> {

    public Attendance findByAttenId(Integer attenId);

    public List<Attendance> findByEmpID(Integer empId);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.repository;

import com.example.demo.entity.Position;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Lenovo
 */
public interface PositionRepo extends CrudRepository<Position, Integer> {

    public Position findByPosID(Integer posID);
}

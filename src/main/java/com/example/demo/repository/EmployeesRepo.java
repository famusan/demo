/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.repository;

import com.example.demo.entity.Employees;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Lenovo
 */
public interface EmployeesRepo extends CrudRepository<Employees, Integer>{
    
    public Employees findByEmpID(Integer empID);

    public List<Employees> findByPosID(Integer posID);
}
